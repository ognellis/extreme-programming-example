import { Money, Expression, Bank, Sum } from './index';

describe('index.ts', () => {
    test('Умножение долларов должно давать правильный результат', () => {
        const five: Money = Money.dollar(5);
        expect(five.times(2)).toEqual(Money.dollar(10));
        expect(five.times(3).times(2)).toEqual(Money.dollar(30));
    });

    test('Доллары с одинаковом количестве равны', () => {
        expect(Money.dollar(5).equals(Money.dollar(5))).toBe(true);
        expect(Money.dollar(5).equals(Money.dollar(6))).toBe(false);
    });

    test('Одинаковое количество долларов и франков не должны быть равны', () => {
        expect(Money.franc(5).equals(Money.dollar(5))).toBe(false);
    });

    test('Проверка валюты', () => {
        expect(Money.dollar(1).getCurrency()).toBe('USD');;
        expect(Money.franc(1).getCurrency()).toBe('CHF');;
    });

    test('Простое сложение', () => {
        const five = Money.dollar(5);
        const sum: Expression = five.plus(five);
        const bank = new Bank();
        const reduced = bank.reduce(sum, "USD")
        expect(reduced).toEqual(Money.dollar(10));
    });

    test('Функция сложения возвращает Sum', () => {
        const five: Money = Money.dollar(5);
        const result: Expression = five.plus(five);
        const sum: Sum = result as Sum;
        expect(sum.augend).toEqual(five);
        expect(sum.addend).toEqual(five);
    });

    test('Получение значение из суммы', () => {
        const sum: Expression = new Sum(Money.dollar(3), Money.dollar(4));
        const bank: Bank = new Bank();
        const result: Money = bank.reduce(sum, "USD");
        expect(result).toEqual(Money.dollar(7));
    });

    test('Получение через reduce значения из Money', () => {
        const bank: Bank = new Bank();
        const result: Money = bank.reduce(Money.dollar(1), "USD");
        expect(result).toEqual(Money.dollar(1));
    });

    test('Проверка одинаковых валют', () => {
        expect(new Bank().getRate("USD", "USD")).toBe(1);
    });

    test('Сокращение денег в другую валюту', () => {
        const bank: Bank = new Bank();
        bank.addRate("CHF", "USD", 2);
        const result: Money = bank.reduce(Money.franc(2), "USD");
        expect(result).toEqual(Money.dollar(1));
    });

    test('Сложение разных валют', () => {
        const fiveBucks: Expression = Money.dollar(5);
        const tenFrancs: Expression = Money.franc(10);
        const bank: Bank = new Bank();
        bank.addRate("CHF", "USD", 2);
        const result: Money = bank.reduce(fiveBucks.plus(tenFrancs), "USD");
        expect(result).toEqual(Money.dollar(10));
    });

    test('Прибавление к сумме денег', () => {
        const fiveBucks: Expression = Money.dollar(5);
        const tenFrancs: Expression = Money.franc(10);
        const bank: Bank = new Bank();
        bank.addRate("CHF", "USD", 2);
        const sum: Expression = new Sum(fiveBucks, tenFrancs).plus(fiveBucks);
        const result: Money = bank.reduce(sum, "USD");
        expect(result).toEqual(Money.dollar(15));
    });

    test('Умножение суммы', () => {
        const fiveBucks: Expression = Money.dollar(5);
        const tenFrancs: Expression = Money.franc(10);
        const bank: Bank = new Bank();
        bank.addRate("CHF", "USD", 2);
        const sum: Expression = new Sum(fiveBucks, tenFrancs).times(2);
        const result: Money = bank.reduce(sum, "USD");
        expect(result).toEqual(Money.dollar(20));
    });
});