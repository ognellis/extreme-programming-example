
export interface Expression {
    reduce(bank: Bank, to: string): Money;
    plus(addend: Expression): Expression;
    times(multiplayer: number): Expression 
};

export class Money implements Expression {
    protected currency: string;
    public amount: number;

    constructor(amount: number, currency: string) {
        this.amount = amount;
        this.currency = currency;
    }

    static dollar(amount: number): Money {
        return new Money(amount, "USD");
    }
    
    static franc(amount: number): Money {
        return new Money(amount, "CHF");
    }

    public equals(object: Money) {
        return this.amount === object.amount && this.currency === object.currency;
    }

    public times(multiplayer: number): Expression {
        return new Money(this.amount * multiplayer, this.currency);
    }

    public getCurrency() {
        return this.currency;
    }

    public plus(addend: Expression): Expression {
        return new Sum(this, addend);
    }

    public reduce(bank: Bank, to: string): Money {
        const rate: number = bank.getRate(this.currency, to);
        return new Money(this.amount / rate, to);
    }
}

export class Bank {
    private rates = {};

    public addRate(from: string, to: string, rate: number) {
        const pair = new Pair(from, to);
        this.rates[pair.getHashCode()] = rate;
    }

    public reduce(source: Expression, to: string): Money {
        return source.reduce(this, to);
    }

    public getRate(from: string, to: string): number {
        if (from === to) {
            return 1;
        }
        const pair = new Pair(from, to);
        const rateKey = pair.getHashCode();
        const rate = this.rates[rateKey];
        return rate;
    }
}

class Pair {
    private from: string;
    private to: string;

    constructor(from: string, to: string) {
        this.from = from;
        this.to = to;
    }

    public equals(pair: Pair): boolean {
        return this.from == pair.from && this.to === pair.to;
    }

    public getHashCode(): string {
        return `${this.from}_${this.to}`;
    }
}

export class Sum implements Expression {
    public augend: Expression;
    public addend: Expression;

    constructor(augend: Expression, addend: Expression) {
        this.augend = augend;
        this.addend = addend;
    }

    public plus(addend: Expression): Expression {
        return new Sum(this, addend);
    }

    public times(multiplayer: number): Expression {
        return new Sum(this.augend.times(multiplayer), this.addend.times(multiplayer));
    }

    public reduce(bank: Bank, to: string): Money {
        const amount: number = this.augend.reduce(bank, to).amount + this.addend.reduce(bank, to).amount;
        return new Money(amount, to);
    }
}